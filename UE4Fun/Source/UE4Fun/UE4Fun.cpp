// Copyright Epic Games, Inc. All Rights Reserved.

#include "UE4Fun.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UE4Fun, "UE4Fun" );
